﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using AuthorizationPortal.Manager.Data;
using ApprendaAPIClient.Models.DeveloperPortal;

namespace AzPManager.ViewModels
{
    public class DeploymentModel : BindableBase, IViewControl
    {
        public MasterContext ModelContext { get; set; }

        public TaskReportCard ModelTasks { get; set; }

        public ICommand ActionCommand { get; set; }

        private bool _isTest;
        private string _validationMessage;

        public DeploymentModel(MasterContext context, bool isTest = false)
        {
            ModelContext = context;
            _isTest = isTest;

            ModelTasks = new TaskReportCard();

            _validationMessage = "";

            // Separate and assign the action button to perform the proper workflow:
            if (ModelContext.DeploymentWorkflow == AuthzWorkflow.New)
            {
                ActionCommand = new RelayCommand(x => DeploymentAction());
            }
            else if (ModelContext.DeploymentWorkflow == AuthzWorkflow.Upgrade)
            {
                ActionCommand = new RelayCommand(x => UpgradeAction());
            }

            ModelContext.Log("Starting the DeploymentModel Workflow.");
        }

        public async void DeploymentAction()
        {
            // New deployment workflow:
            ValidationMessage = "";

            ModelContext.Log("Starting New Deployment of AzP.");
            var alias = "authorization";
            var version = "v1";

            ModelTasks = new TaskReportCard("enableauth", "create", "promote", "sync", "verifyreg", "enableeus");

            // 1.) Enable External Authentication:
            if (!ModelTasks.GetTaskResult("enableauth"))
            {
                // Attempt to upload and enable the Auth Plugin:
                if (!(await ModelContext.Controller.EnableExternalAuth(ModelContext.AzpApplicationDetails.AuthzArchives["auth"].FullName)))
                {
                    ValidationMessage = "Failed to enable External Authentication, either try again or perform the function manually in the SOC -> Configuration -> Security.";
                    return;
                }

                // Successfully created the Auth Plugin, mark task as complete:
                ModelTasks.SetTaskResult("enableauth", true);
            }

            // 2.) Create Core application with Alias 'authorization' & Promote to 'Published':

            if (!ModelTasks.GetTaskResult("create"))
            {
                // Create the application:
                if (!(await ModelContext.Controller.CreateApplication(alias)))
                {
                    ValidationMessage = "Failed to deploy the application, please try again.";
                    return;
                }

                // Patch the application:
                if (!(await ModelContext.Controller.PatchApplication(alias, version, ModelContext.AzpApplicationDetails.AuthzZips["core"])))
                {
                    ValidationMessage = "Failed to patch the application, please try again.";
                    return;
                }

                // Successfully created the application, mark task:
                ModelTasks.SetTaskResult("create", true);
            }

            // Promote the application:
            if (!ModelTasks.GetTaskResult("promote"))
            {
                if (!(await ModelContext.Controller.PromoteApplication(alias, version, ApplicationVersionStage.Published, true)))
                {
                    ValidationMessage = "Failed to promote the application, please try again.";
                    await RollBack(alias);
                    return;
                }

                // Successfully promoted the application, mark task:
                ModelTasks.SetTaskResult("promote", true);
            }

            // 3.) Perform Initial Tenant/User sync with AzP:
            if (!ModelTasks.GetTaskResult("sync"))
            {
                // Sync the initial data from SOC to AzP:
                if (!(await ModelContext.Controller.SyncInitialAuthzData(alias)))
                {
                    ValidationMessage = "Failed to Synchronize initial data from SOC to AzP.";
                    return;
                }

                // Successfully sync initial data, mark task:
                ModelTasks.SetTaskResult("sync", true);
            }

            // 4.) Verify Registry settings enable EUS:
            if (!ModelTasks.GetTaskResult("enableeus"))
            {
                // Verify Registry settings before continuing:
                if (!(await ModelContext.Controller.VerifyRegistry()))
                {
                    ValidationMessage = "Failed to check or modify registry settings, either try again or perform this function manually in the SOC -> Configuration -> Platform Registry.";
                    return;
                }

                // Deploy the EUS:
                if (!(await ModelContext.Controller.DeployEndUserStore(ModelContext.AzpApplicationDetails.AuthzZips["eus"])))
                {
                    ValidationMessage = "Failed to deploy the EUS, either try again or perform this step manually in the SOC -> Configuration -> User Store.";
                    return;
                }

                // Successfully enabled the eus, mark task:
                ModelTasks.SetTaskResult("enableeus", true);
            }

            // 5.) Flush the Cache:
            if (!(await ModelContext.Controller.FlushCache()))
            {
                ValidationMessage = "Failed to flush the cache, this isn't critical and can continue.";
            }

        }

        public async void UpgradeAction()
        {
            // Upgrade deployment workflow:

            ModelTasks = new TaskReportCard("enableauth", "create", "promote", "sync", "verifyreg", "enablepatcheus", "corepatch", "enablecoreeus");


            // 1.) Create the Core app for patching & Promote:

        }

        private async Task RollBack(string alias)
        {
            if (!(await ModelContext.Controller.DeleteApplication(alias)))
            {
                ValidationMessage = $"Error occurred during rollback. It's recommended to manually delete the <{alias}> application from <{ModelContext.Controller.PlatformTenant}> Tenant.";
            }
        }

        public string ValidationMessage
        {
            get { return _validationMessage; }
            set
            {
                ModelContext.Log(value, LogLevel.Fatal);
                SetProperty(ref _validationMessage, value);
                RaisePropertyChanged();
            }
        }

        public bool IsTest
        {
            get { return _isTest; }
        }
    }
}
