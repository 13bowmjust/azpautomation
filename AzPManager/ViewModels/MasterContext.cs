﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzPManager.Controls;
using AuthorizationPortal.Manager;
using AuthorizationPortal.Manager.Data;
using Prism.Mvvm;
using System.IO;
using System.Windows.Input;

namespace AzPManager.ViewModels
{
    public class MasterContext : BindableBase
    {
        public const bool IsTest = false;

        private IViewControl _currentView;
        private List<IViewControl> _viewControls;

        public ICommand DisposeCommand { get; set; }

        private List<string> _executionLog;

        private AuthzWorkflow _workflow;
        private AuthzAppDetails _azpDetails;

        private PortalControl _platformController;

        public MasterContext()
        {
            // Initialize Context:
            _platformController = new PortalControl();

            // Assume the workflow will be a new deployment:
            _azpDetails = new AuthzAppDetails();
            _workflow = AuthzWorkflow.New;

            _executionLog = new List<string>();
            _viewControls = new List<IViewControl>();

            // If testing new functionality with the TestView, change const to TRUE.
            if (!IsTest)
            {
                // Instansiate ViewModels:
                _viewControls.Add(new LoginModel(this));
                _viewControls.Add(new DataEntryModel(this));
                _viewControls.Add(new DeploymentModel(this));
            }
            else
            {
                _viewControls.Add(new LoginModel(this, true));
                _viewControls.Add(new DataEntryModel(this, true));
                _viewControls.Add(new DeploymentModel(this, true));
                _viewControls.Add(new TestModel(this, true));
            }

            DisposeCommand = new RelayCommand(x => Dispose());

            // Set the initial view:
            CurrentView = _viewControls[0];
        }

        ~MasterContext()
        {
            _azpDetails.Dispose();
            _platformController.Disconnect();
        }

        public void Dispose()
        {
            SaveLogs();

            _azpDetails.Dispose();
            _platformController.Disconnect();
            _viewControls = new List<IViewControl>();
            _executionLog = new List<string>();

        }

        public void NavigateNext()
        {
            var index = _viewControls.IndexOf(CurrentView);

            if (index < _viewControls.Count)
            {
                CurrentView = _viewControls[++index];
            }
        }

        public void NavigateLast()
        {
            var index = _viewControls.IndexOf(CurrentView);

            if (index > 0)
            {
                CurrentView = _viewControls[--index];
            }
        }

        public void Log(string value, LogLevel level = LogLevel.Info)
        {
            // Add the string value to the execution log using Log Format (extension in Helper):
            _executionLog.Add(value.ToLogFormat(level));
        }

        public async void SaveLogs()
        {
            // Save logs to an output file:
            using (var writer = new StreamWriter("AzPManager.log", true))
            {
                foreach (var log in _executionLog)
                {
                    await writer.WriteLineAsync(log);
                }

                writer.Flush();
                writer.Close();
            }
        }

        public void SetWorkflow(AuthzWorkflow workflow)
        {
            _workflow = workflow;
        }

        public IViewControl CurrentView
        {
            get { return _currentView; }
            set
            {
                if (_currentView != value)
                {
                    SetProperty(ref _currentView, value);
                    RaisePropertyChanged();
                }
            }
        }

        public AuthzWorkflow DeploymentWorkflow
        {
            get { return _workflow; }
        }

        public AuthzAppDetails AzpApplicationDetails
        {
            get { return _azpDetails; }
            set { _azpDetails = value; }
        }

        public PortalControl Controller
        {
            get { return _platformController; }
            set { _platformController = value; }
        }
    }
}
