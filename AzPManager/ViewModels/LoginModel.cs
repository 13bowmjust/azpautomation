﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xceed.Wpf.Toolkit;
using AuthorizationPortal.Manager.Data;

namespace AzPManager.ViewModels
{
    public class LoginModel : BindableBase, IViewControl
    {
        public MasterContext ModelContext { get; set; }

        private bool _isTest;

        private string _validationMessage;

        private string _username;
        private string _tenant;
        private string _platformUrl;

        public ICommand LoginCommand { get; set; }

        public LoginModel(MasterContext context, bool isTest = false)
        {
            ModelContext = context;
            _isTest = isTest;

            _username = "";
            _tenant = "";
            _platformUrl = "";

            _validationMessage = "";

            LoginCommand = new RelayCommand(x => Login(x));

            ModelContext.Log("Starting the LoginModel Workflow.");
        }

        private async void Login(object password)
        {
            // Quick check to see that data is valid:
            if ((!string.IsNullOrEmpty(_username) && _username.Contains("@")) &&
                (!string.IsNullOrEmpty(_tenant) && !_tenant.Any(char.IsWhiteSpace)) &&
                (!string.IsNullOrEmpty(_platformUrl) && _platformUrl.Contains("https://")))
            {
                // Set the controller context with user data:
                ModelContext.Controller = new AuthorizationPortal.Manager.PortalControl(_platformUrl, _username, ((WatermarkPasswordBox)password).Password, _tenant);

                // Connect to the platform:
                ModelContext.Log("Connecting to the platform with specified credentials.");
                var result = await ModelContext.Controller.Connect();

                if (!result)
                {
                    ValidationMessage = "An error occured on login, Check credentials and try again.";
                }

                // Get Workflow:
                if ((await ModelContext.Controller.VerifyAppExists("authorization")))
                {
                    // Authorization Portal exists, meaning this will be an upgrade:
                    ModelContext.SetWorkflow(AuthzWorkflow.Upgrade);
                }
                else
                {
                    // This is implied but, precaution to be certain the workflow is set correctly:
                    ModelContext.SetWorkflow(AuthzWorkflow.New);
                }

                // All is well and the controller is connected, move to next workflow:
                ModelContext.NavigateNext();
            }
        }

        public string Username
        {
            get { return _username; }
            set
            {
                SetProperty(ref _username, value);
                RaisePropertyChanged();
            }
        }

        public string Tenant
        {
            get { return _tenant; }
            set
            {
                SetProperty(ref _tenant, value);
                RaisePropertyChanged();
            }
        }

        public string PlatformUrl
        {
            get { return _platformUrl; }
            set
            {
                SetProperty(ref _platformUrl, value);
                RaisePropertyChanged();
            }
        }

        public string ValidationMessage
        {
            get { return _validationMessage; }
            set
            {
                ModelContext.Log(value, LogLevel.Fatal);
                SetProperty(ref _validationMessage, value);
                RaisePropertyChanged();
            }
        }

        public bool IsTest
        {
            get { return _isTest; }
        }
    }
}
