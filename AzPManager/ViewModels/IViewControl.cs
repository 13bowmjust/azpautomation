﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzPManager.ViewModels
{
    public interface IViewControl
    {

        MasterContext ModelContext { get; set; }

        string ValidationMessage { get; set; }

        bool IsTest { get; }

    }
}
