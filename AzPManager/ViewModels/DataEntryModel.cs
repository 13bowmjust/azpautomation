﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.IO;
using System.IO.Compression;
using AuthorizationPortal.Manager.Data;

namespace AzPManager.ViewModels
{
    public class DataEntryModel : BindableBase, IViewControl
    {
        public MasterContext ModelContext { get; set; }

        private bool _isTest;

        private const string TempDirectory = @"C:\Temp\Apprenda\AzPStaging\";

        private string _validationMessage;

        private string _azpDirectory;

        // TODO: Can be used in the VerifyConfig() method to let the UI create the config
        // values to be used with this AzP deployment.
        private string _azpHeader;
        private string _azpSecret;
        private string _azpServiceUser;
        private string _azpSyncUser;
        private string _azpAlias;

        public ICommand OpenDirCommand { get; set; }
        public ICommand ValidateCommand { get; set; }

        public DataEntryModel(MasterContext context, bool isTest = false)
        {
            ModelContext = context;

            _isTest = isTest;

            _validationMessage = "";
            _azpDirectory = "";

            OpenDirCommand = new RelayCommand(x => OpenDirectory());
            ValidateCommand = new RelayCommand(x => Validate());

            ModelContext.Log("Starting the DataEntryModel Workflow.");
        }

        public void OpenDirectory()
        {
            // Get the directory that contains AzP Files:
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                // Set the directory that was selected:
                ChosenDirectory = dialog.SelectedPath;
            }
        }

        public void Validate()
        {
            // Remove message if trying again:
            ValidationMessage = "";

            // Validate that the archives needed are here:
            var neededZips = Helper.AuthzWorkflowData[(int)ModelContext.DeploymentWorkflow];
            ModelContext.Log($"Required archives for the <{ModelContext.DeploymentWorkflow}> workflow: [{string.Join(", ", neededZips)}]");
            var directories = new List<string>();
            var zips = new List<FileInfo>();

            // Verify that all zips exist:
            try
            {
                ModelContext.Log($"Getting Zip Archives from directory <{_azpDirectory}>");
                zips = Directory.GetFiles(_azpDirectory, "*.zip", SearchOption.TopDirectoryOnly).Select(file => new FileInfo(file)).Where(x => x.Name.ContainsAny(neededZips)).ToList();
            }
            catch (Exception ex)
            {
                ValidationMessage = ex.Message;
                return;
            }
            
            if (ModelContext.DeploymentWorkflow == AuthzWorkflow.New)
            {
                zips.RemoveAll(fi => fi.Name.Contains("Patch"));
            }

            if (zips.ToArray().Length != neededZips.Length)
            {
                ValidationMessage = "The required zip files are not present, please isolate them into a folder and try again.";
                return;
            }

            // Copy zips to TempDirectory and Unzip:
            foreach (var azpZip in zips)
            {
                var outputDirectory = "";

                // Unzip the archive from the list of zips:
                if (!UnzipAzpArchive(azpZip, out outputDirectory))
                {
                    ValidationMessage = $"Unable to unzip '{azpZip}', please try again.";
                    return;
                }

                // If unzip is successful, add the output path:
                directories.Add(outputDirectory);
            }

            // Build an AuthzAppDetails instance to hold needed data:
            ModelContext.AzpApplicationDetails = new AuthzAppDetails(neededZips, directories.ToArray());
            ModelContext.Log($"<AzPApplicationDetails> instance created.");

            // Verify that all web.configs have matching data for:
            // Sync and Service User, Shared Secret, Auth Header, Alias, etc;
            // TODO: Add validation logic for web.config files.
            //VerifyConfigFiles(ModelContext.AzpApplicationDetails.AuthzArchives.Values.ToArray());

            // Build the Archives and get final details into AuthzAppDetails:
            foreach (var archive in ModelContext.AzpApplicationDetails.AuthzArchives)
            {
                var finalArchive = ZipAzpArchive(archive.Value);

                if (!string.IsNullOrEmpty(finalArchive))
                {
                    // If the final archive creation was successful, add the zip to the AuthzAppDetails:
                    ModelContext.AzpApplicationDetails.AddZip(archive.Key, finalArchive);
                }
            }

            // Final Verification:
            if (!(ModelContext.AzpApplicationDetails.AuthzZips.Count == neededZips.Count()))
            {
                ValidationMessage = "Error, required zip archives are not present. Try Validation again.";
                return;
            }

            // Test Mode:
            if (IsTest)
            {
                ModelContext.AzpApplicationDetails.Dispose();
            }

            // If all is well, continue to deployment:
            ModelContext.Log("Validation Completed, moving on to the DeploymentModel workflow.");
            ModelContext.NavigateNext();
        }

        private bool UnzipAzpArchive(FileInfo sourceZip, out string directory)
        {
            try
            {
                // Unzip the archive to the temp directory:
                directory = $@"{TempDirectory}\working\{sourceZip.Name}";
                ZipFile.ExtractToDirectory(sourceZip.FullName, directory);
                ModelContext.Log($"Unzipping <{sourceZip.Name}> to directory <{directory}>");
            }
            catch (Exception ex)
            {
                ValidationMessage = ex.Message;
                directory = "";
                return false;
            }

            return true;
        }

        private string ZipAzpArchive(DirectoryInfo sourceDirectory)
        {
            // Get the final Archive Path once zipped:
            var archivePath = $@"{TempDirectory}\{sourceDirectory.Name}";

            try
            {
                // Zip directory into archive:
                ZipFile.CreateFromDirectory(sourceDirectory.FullName, archivePath);
                ModelContext.Log($"Zipping directory <{sourceDirectory.FullName}> to archive <{archivePath}>");
            }
            catch (Exception ex)
            {
                ValidationMessage = ex.Message;
                return null;
            }

            return archivePath;
        }

        private bool VerifyConfigFiles(DirectoryInfo[] archives)
        {
            // Get web.config and establish ConfigurationManager with it:


            // 1.) Do the check vars have value? -> Establish and exit:


            // 2.) Check web.config values against check vars:


            // 3.) Return result:

            return true;
        }

        public string ChosenDirectory
        {
            get { return _azpDirectory; }
            set
            {
                SetProperty(ref _azpDirectory, value);
                RaisePropertyChanged();
            }
        }

        public string ValidationMessage
        {
            get { return _validationMessage; }
            set
            {
                ModelContext.Log(value, LogLevel.Fatal);
                SetProperty(ref _validationMessage, value);
                RaisePropertyChanged();
            }
        }

        public bool IsTest
        {
            get { return _isTest; }
        }
    }
}
