﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ApprendaAPIClient.Models.DeveloperPortal;
using Prism.Mvvm;

namespace AzPManager.ViewModels
{
    public class TestModel : BindableBase, IViewControl
    {
        public MasterContext ModelContext { get; set; }

        private bool _isTest;

        private string _validationMessage;

        private int _burnTesting;

        private ObservableCollection<string> _resultsView;

        public ICommand TestCommand { get; set; }

        public TestModel()
        {
            _validationMessage = "";
            _resultsView = new ObservableCollection<string>();
        }

        public TestModel(MasterContext context, bool isTest = false)
        {
            ModelContext = context;
            _isTest = isTest;

            _burnTesting = 1;

            _validationMessage = "";
            _resultsView = new ObservableCollection<string>();

            TestCommand = new RelayCommand(x => Test());
        }

        public async void Test()
        {
            for (var count = 0; count < _burnTesting; count++)
            {
                TestResults.Add($"--------------------------------------Begin Burn Test: {count}--------------------------------------");

                ModelContext.Controller = new AuthorizationPortal.Manager.PortalControl("https://apps.apprenda.jbowman", "jbowman@appjb.local", "password", "bowmanappr");
                var result = await ModelContext.Controller.Connect();

                TestResults.Add("PortalController Created and Connected: " + result);

                var testResult = (await ModelContext.Controller.Test());
                TestResults.Add("Repo Test: " + testResult); // Expect Value

                TestResults.Add("Session Token: " + ModelContext.Controller.ApprendaSessionToken);
                TestResults.Add("ACP Version: " + ModelContext.Controller.ApprendaVersion);

                var regtestResult = (await ModelContext.Controller.VerifyRegistry());
                TestResults.Add("Reg Test: " + regtestResult); // Expect True;

                var authtestResult = (await ModelContext.Controller.EnableExternalAuth(@"E:\WorkingFolder\PlatformAuthZ\152-Debug\AzP-Auth"));
                TestResults.Add("ExternAuth Test: " + authtestResult); // Expect True;

                var flushtestResult = (await ModelContext.Controller.FlushCache());
                TestResults.Add("Flush Test: " + flushtestResult);  // Expect True;

                var appexistsResult = (await ModelContext.Controller.VerifyAppExists("authorization"));
                TestResults.Add("AzP Exists Test: " + appexistsResult); // Expect True;

                var appexistsDummyResult = (await ModelContext.Controller.VerifyAppExists("azp"));
                TestResults.Add("AzP False Exists Test: " + appexistsDummyResult); // Expect False;

                if (appexistsDummyResult)
                {
                    var appDeleteTest = (await ModelContext.Controller.DeleteApplication("azp"));
                    TestResults.Add("AzP App Delete: " + appDeleteTest); // Expect True;
                }

                var appCreateTest = (await ModelContext.Controller.CreateApplication("azp"));
                TestResults.Add("AzP App Create: " + appCreateTest); // Expect True;

                var appPatchTest = (await ModelContext.Controller.PatchApplication("azp", "v1",
                    new System.IO.FileInfo(@"E:\WorkingFolder\PlatformAuthZ\152-Debug\AzP-Core\AzPCore-Test.zip")));
                TestResults.Add("AzP App Patch: " + appPatchTest); // Expect True;

                var appPromoteTest = (await ModelContext.Controller.PromoteApplication("azp", "v1", ApplicationVersionStage.Published, true));
                TestResults.Add("AzP Promote to Published: " + appPromoteTest); // Expect True;

                var appInitTest = (await ModelContext.Controller.SyncInitialAuthzData("azp"));
                TestResults.Add("AzP Init Details: " + appInitTest);

                //var appSyncTest = (await ModelContext.Controller.SyncAuthzPortals("authorization", "azp"));
                //TestResults.Add("AzP Sync Result: " + appSyncTest); // Expect True;

                //var appDeleteTest = (await ModelContext.Controller.DeleteApplication("azp"));
                //TestResults.Add("AzP App Delete: " + appDeleteTest); // Expect True;

                ModelContext.Controller.Disconnect();

                TestResults.Add("Disconnected.");

                ModelContext = new MasterContext();

                TestResults.Add($"--------------------------------------End Burn Test: {count}--------------------------------------");
            }
        }

        public ObservableCollection<string> TestResults
        {
            get { return _resultsView; }
            set
            {
                SetProperty(ref _resultsView, value);
                RaisePropertyChanged();
            }
        }

        public string ValidationMessage
        {
            get { return _validationMessage; }
            set
            {
                SetProperty(ref _validationMessage, value);
                RaisePropertyChanged();
            }
        }

        public bool IsTest
        {
            get { return _isTest; }
        }
    }
}
