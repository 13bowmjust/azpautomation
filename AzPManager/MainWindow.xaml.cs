﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AuthorizationPortal.Manager;
using AzPManager.ViewModels;

namespace AzPManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //private async void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    var controller = new PortalControl("https://apps.apprenda.jbowman", "jbowman@appjb.local", "password", "bowmanappr");
        //    var result = await controller.Connect();

        //    var testResult = (await controller.Test());
        //    var regtestResult = (await controller.VerifyRegistry());
        //    var authtestResult = (await controller.EnableExternalAuth(@"E:\WorkingFolder\PlatformAuthZ\152-Debug\AzP-Auth"));
        //    var flushtestResult = (await controller.FlushCache());
        //    var appexistsResult = (await controller.VerifyAppExists("authorization"));
        //    var appexistsDummyResult = (await controller.VerifyAppExists("azp")); // doesn't exist;

        //    lstItems.Items.Add("Repo Test: " + testResult);
        //    lstItems.Items.Add("Reg Test: " + regtestResult);
        //    lstItems.Items.Add("ExternAuth Test: " + authtestResult);
        //    lstItems.Items.Add("Flush Test: " + flushtestResult);
        //    lstItems.Items.Add("AzP Exists Test: " + appexistsResult);
        //    lstItems.Items.Add("AzP False Exists Test: " + appexistsDummyResult);
        //    lstItems.Items.Add("Session Token: " + controller.ApprendaSessionToken);
        //    lstItems.Items.Add("ACP Version: " + controller.ApprendaVersion);
        //}
    }
}
