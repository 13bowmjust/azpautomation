﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApprendaAPIClient;
using ApprendaAPIClient.Factories;
using ApprendaAPIClient.Clients;
using AuthorizationPortal.Manager.Data;
using RestSharp;
using System.IO;
using System.Net;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Tenants;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Locations;
using Apprenda.ClientServices.AuthorizationPortal.Domain.TenantTypes;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Groups;
using AzPUser = Apprenda.ClientServices.AuthorizationPortal.Domain.Users.User;
using ApprendaAPIClient.Models.DeveloperPortal;
using Newtonsoft.Json;

namespace AuthorizationPortal.Manager
{
    public class PortalControl
    {

        private IApprendaApiClient _acpClient;
        private string _acpSessionToken;
        private string _acpVersion;
        private RestClient _acpRestClient;

        public string PlatformUrl { get; set; }
        public string PlatformUser { get; set; }
        public string PlatformPassword { get; set; }
        public string PlatformTenant { get; set; }

        public PortalControl()
        {
            PlatformUrl = "";
            PlatformUser = "";
            PlatformPassword = "";
            PlatformTenant = "";
        }

        public PortalControl(string url, string user, string password, string tenant = null)
        {
            PlatformUrl = url;
            PlatformUser = user;
            PlatformPassword = password;
            PlatformTenant = tenant;

            _acpClient = null;
            _acpSessionToken = "";
            _acpRestClient = null;
            _acpVersion = "";
        }

        public async Task<bool> Connect()
        {
            try
            {
                var clientFact = new ApprendaApiClientFactory(PlatformUrl, PlatformUser, PlatformPassword);
                _acpClient = clientFact.GetV1Client();

                // Login with Tenant and save Session Token: (Additional non-client REST calls.)
                _acpSessionToken = (await _acpClient.Login(PlatformUser, PlatformPassword, PlatformTenant));

                // Get the ACP Version from a Host on-platform:
                _acpVersion = $"v{(await _acpClient.GetAllHosts()).First().PlatformVersion}";

                if (string.IsNullOrEmpty(_acpSessionToken))
                {
                    return false;
                }

                _acpRestClient = new RestClient(PlatformUrl);
                _acpRestClient.AddDefaultHeader("ApprendaSessionToken", _acpSessionToken);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public async void Disconnect()
        {
            if (_acpClient != null &&
                !string.IsNullOrEmpty(_acpSessionToken))
            {
                await _acpClient.Logout(_acpSessionToken);
                _acpClient = null;
                _acpSessionToken = "";
                _acpRestClient = null;
            }
        }

        public async Task<bool> FlushCache()
        {
            // GET CacheService.asmx:
            var result = await MakeRestRequest("soc/CacheService.asmx/FlushAllCaches", Method.POST);

            if (result == null)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> VerifyRegistry()
        {
            return (await CheckSetting("Platform.ExternalUserStoreRoleAssignment") && await CheckSetting("Platform.ExternalUserStoreRoleCreation"));
        }

        public async Task<bool> VerifyAppExists(string appAlias)
        {
            try
            {
                var result = await _acpClient.GetApplication(appAlias);

                if (result != null && result.Alias == appAlias)
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        public async Task<bool> EnableExternalAuth(string azpDirectory)
        {
            // Enable External Auth in the SOC:
            var authRegSetting = (await _acpClient.GetRegistrySetting("Authentication.ExternalProvider.Headers"));
            var repoSetting = (await _acpClient.GetRegistrySetting("ApplicationRepositoryRoot"));
            var repoLocation = repoSetting.Value.Substring(repoSetting.Value.LastIndexOf("\\\\")).Replace('@', '\\');

            // Check if ExternalAuth is enabled:
            if (string.IsNullOrEmpty(authRegSetting.Value))
            {
                // 1.) Set the Registry Setting for Authentication.ExternalProvider.Headers -> 'AzP-Service-Auth':
                authRegSetting.Value = "AzP-Service-Auth";
                var regResult = (await _acpClient.UpdateRegistrySetting(authRegSetting));

                if (!regResult)
                {
                    return false;
                }

                // 2.) Copy AzP-Auth contents to repo '\\repo\Applications\__PluginRepository\apprenda\authentication\v8.2.0\base\Services\Citadel\ExternalAuthn:
                var copyResult = Helper.Copy(azpDirectory, $@"{repoLocation}__PluginRepository\apprenda\authentication\v8.2.0\base\Services\Citadel\ExternalAuthn");

                if (!copyResult)
                {
                    return false;
                }

                // 3.) Flush the Cache:
                await FlushCache();

                // 4.) Move all Citadel Services to ensure the Auth Plugin is set:
                var workloads = (await _acpClient.GetWorkloads("authentication", _acpVersion)).Where(workload => workload.ArtifactName == "Apprenda.SaaSGrid.Citadel.Service.CitadelService");

                foreach (var citadelService in workloads)
                {
                    var moveResult = (await _acpClient.RelocateWorkload(citadelService.DeployedArtifactId));

                    if (!moveResult)
                    {
                        return false;
                    }
                }
            }

            // If the Plugin is enabled, continue:
            return true;
        }

        public async Task<bool> CheckEndUserStoreStatus()
        {
            var eusRegSetting = (await _acpClient.GetRegistrySetting("Platform.ExternalUserStoreEnabled"));

            if (!bool.Parse(eusRegSetting.Value.ToLower()))
            {
                return false;
            }

            return true;
        }

        private async Task<bool> EnableEndUserStore()
        {
            var result = await MakeRestRequest("soc/api/v1/externaluserstore/enable", Method.POST);

            if (result == null)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> DeployEndUserStore(FileInfo azpEus)
        {
            var result = await MakeRestRequest("soc/api/v1/externaluserstore",
                Method.PUT,
                fileName: azpEus.Name,
                filePath: azpEus.FullName,
                codes: new HttpStatusCode[] { HttpStatusCode.NoContent, HttpStatusCode.BadRequest });

            if (result == null)
            {
                return false;
            }

            var enableResult = await EnableEndUserStore();

            return enableResult;
        }

        public async Task<bool> CheckSetting(string settingName)
        {
            var setting = (await _acpClient.GetRegistrySetting(settingName));

            // Platform.ExternalUserStore* -> Exact:
            if (!(setting.Value == "Exact"))
            {
                setting.Value = "Exact";
                return (await _acpClient.UpdateRegistrySetting(setting));
            }

            return true;
        }

        public async Task<bool> CreateApplication(string alias)
        {
            // Put Data together to upload a new application:
            var result = await MakeRestRequest("developer/api/v1/apps",
                Method.POST,
                body: new { Alias = alias, Name = alias, Description = "" });

            if (result == null)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> PatchApplication(string alias, string version, FileInfo azpArchive)
        {
            // Patch the application:
            var result = await _acpClient.PatchVersion(alias,
                version,
                true,
                File.ReadAllBytes(azpArchive.FullName),
                async: true);

            if (result == null)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> DeleteApplication(string alias)
        {
            // Delete the application by Alias:
            return await _acpClient.DeleteApplication(alias);
        }

        public async Task<bool> PromoteApplication(string alias, string version, ApplicationVersionStage stage, bool wait = false)
        {
            // Promote the application to the desired stage:
            if (wait)
            {
                var result = await _acpClient.PromoteVersion(alias, version, stage, async: false);

                if (!result)
                {
                    return false;
                }

                return await WaitForPromotion(alias, version);
            }
            else
            {
                return await _acpClient.PromoteVersion(alias, version, stage, async: false);
            }
        }

        public async Task<bool> WaitForPromotion(string alias, string version)
        {
            var isLive = false;
            var checkApiLive = true;
            var checkAppPromoted = false;

            while (!isLive)
            {
                if (!checkAppPromoted)
                {
                    var result = await MakeRestRequest($"developer/api/v1/versions/{alias}/{version}/reportCards/latest", Method.GET);

                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        var reportCard = PlatReportCard.FromJson(result.Content);

                        if (reportCard[0].Success)
                        {
                            checkAppPromoted = true;
                            checkApiLive = false;
                        }
                    }
                }

                if (!checkApiLive)
                {
                    var result = await MakeRestRequest($"{alias}/api/v1/tenants", Method.GET);

                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        if (result.Content == "[]")
                        {
                            checkApiLive = true;
                        }
                    }
                }

                isLive = checkApiLive && checkAppPromoted;
            }

            return true;
        }

        public async Task<bool> SyncAuthzPortals(string mainAzp, string copyAzp)
        {
            // Create the Authz Team:
            var azpUser = await GetUser(mainAzp, PlatformUser);

            if (azpUser == null)
            {
                azpUser = new AzPUser()
                {
                    Email = PlatformUser,
                    Id = PlatformUser,
                    Description = " ",
                    FirstName = " ",
                    LastName = " ",
                    MiddleName = " ",
                    Prefix = " ",
                    Suffix = " "
                };
            }

            if (!(await CreateAuthzTeamUser(copyAzp, azpUser)))
            {
                return false;
            }

            // Get data from master: (mainAzp)
            using (var azp1 = new AzpApiClient(PlatformUrl, mainAzp))
            {
                // Set the auth token for the AZP Client 1:
                azp1.AddHeader("ApprendaSessionToken", _acpSessionToken);

                using (var azp2 = new AzpApiClient(PlatformUrl, copyAzp))
                {
                    // Set the auth token for the AZP Client 2:
                    azp2.AddHeader("ApprendaSessionToken", _acpSessionToken);

                    // Copy the data:
                    var result = azp2.LoadFrom(azp1, false);

                    // Return the result:
                    return result;
                }
            }
        }

        public async Task<bool> SyncInitialAuthzData(string alias)
        {
            var platTenants = PlatSocTenants.FromJson((await MakeRestRequest("soc/api/v1/tenants?_search=false", Method.GET)).Content);
            var platUsers = PlatSocUsers.FromJson((await MakeRestRequest("soc/api/v1/users?_search=false", Method.GET)).Content);
            var platRoles = await _acpClient.GetRoles();

            // Create the Authz Team:
            var adminUser = await GetUser(alias, PlatformUser);

            if (adminUser == null)
            {
                adminUser = new AzPUser()
                {
                    Email = PlatformUser,
                    Id = PlatformUser,
                    Description = " ",
                    FirstName = " ",
                    LastName = " ",
                    MiddleName = " ",
                    Prefix = " ",
                    Suffix = " "
                };
            }

            // Create AuthzAdmin:
            if (!(await CreateAuthzTeamUser(alias, adminUser)))
            {
                return false;
            }

            // Create Location Data:
            var location = new Location()
            {
                Label = "Default",
                Description = "Default Location"
            };

            var locationResult = await MakeRestRequest($"{alias}/api/v1/locations", Method.POST, location);

            if (locationResult == null)
            {
                return false;
            }

            // Create TenantType Data:
            var tenantType = new TenantType()
            {
                Id = "defaulttype",
                Name = "Default Type",
                Description = "Default TenantType for initial onboarding."
            };

            var tenanttypeResult = await MakeRestRequest($"{alias}/api/v1/tenantTypes", Method.POST, tenantType);

            if (tenanttypeResult == null)
            {
                return false;
            }

            // Add all Tenants from SOC to AzP:
            foreach (var tenant in platTenants.TenantItems)
            {
                // Make Tenant data object:
                var azpTenant = tenant.ToAzPTenant(tenantType, location);

                // Create Admin User First:
                var tenantAdminUser = await GetUser(alias, tenant.AdminUser.Identifier);

                // If the User can't be found in the details, use the current SOC Details:
                if (tenantAdminUser == null)
                {
                    tenantAdminUser = tenant.AdminUser.ToAzPUser();
                }

                // Create the User:
                if (!(await CreateAuthzUser(alias, tenantAdminUser)))
                {
                    return false;
                }

                // Create the Tenant:
                if (!(await CreateAuthzTenant(alias, azpTenant)))
                {
                    return false;
                }

                // Create the TenantUser membership:
                if (!(await CreateAuthzTenantUser(alias, azpTenant.Alias, tenantAdminUser)))
                {
                    return false;
                }

                // Filter out user-tenant memberships that get created as admins:
                //var userIndex = platUsers.Items.FindIndex(user => user.Identifier == tenant.AdminUser.Identifier);
                //platUsers.Items[userIndex].MemberOfTenants.RemoveAll(t => t.Alias == tenant.Alias);
            }

            // Iterate all users from the SOC:
            foreach (var user in platUsers.Items)
            {
                // Get User details into an AzP object:
                var azpUser = user.ToAzPUser();

                if (!(await CreateAuthzUser(alias, azpUser)))
                {
                    return false;
                }

                if (user.MemberOfTenants.Count > 0)
                {
                    // Add Tenants to Main list:
                    foreach (var tenant in user.MemberOfTenants)
                    {
                        if (!(await CreateAuthzTenantUser(alias, tenant.Alias, azpUser)))
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        private async Task<bool> CreateAuthzTeamUser(string alias, AzPUser adminUser)
        {
            // Make request to add user to copyAzp:
            var userResult = await MakeRestRequest($"{alias}/api/v1/users",
                Method.POST,
                adminUser);

            if (userResult == null)
            {
                return false;
            }

            // Make request to add user to $authz$:
            var membershipData = new TenantUser()
            {
                Alias = "$authz$",
                Email = adminUser.Email,
                Id = adminUser.Id,
                Name = $"{adminUser.FirstName} {adminUser.LastName}",
                PortalRoles = "authorizationAdmin"
            };

            var memberResult = await MakeRestRequest($"{alias}/api/v1/tenantusers",
                Method.POST,
                membershipData);

            if (memberResult == null)
            {
                return false;
            }

            return true;
        }

        private async Task<bool> CreateAuthzTenant(string alias, Tenant tenant)
        {
            var tenantResult = await MakeRestRequest($"{alias}/api/v1/tenants", Method.POST, tenant);

            if (tenantResult == null &&
                tenantResult.StatusCode != HttpStatusCode.Created)
            {
                return false;
            }

            return true;
        }

        private async Task<bool> CreateAuthzTenantUser(string alias, string tenantAlias, AzPUser user)
        {
            var tenantUser = new TenantUser()
            {
                Alias = tenantAlias,
                Email = user.Email,
                Id = user.Id,
                Name = $"{user.FirstName} {user.LastName}"
            };

            var tenantUserResult = await MakeRestRequest($"{alias}/api/v1/tenantusers", Method.POST, tenantUser);

            if (tenantUserResult == null &&
                tenantUserResult.StatusCode != HttpStatusCode.Created)
            {
                return false;
            }

            return true;
        }

        private async Task<bool> CreateAuthzUser(string alias, AzPUser user)
        {
            var userResult = await MakeRestRequest($"{alias}/api/v1/users", Method.POST, user);

            if (userResult == null &&
                userResult.StatusCode == HttpStatusCode.Created)
            {
                return false;
            }

            return true;
        }

        private async Task<AzPUser> GetUser(string alias, string search)
        {
            var userRequest = await MakeRestRequest($"{alias}/api/v1/users?search={search}", Method.GET);

            if (userRequest == null ||
                userRequest.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }

            return (AuthzUsersResult.FromJson(userRequest.Content).First());
        }

        public static async Task<string> Test(string url, string user, string password)
        {
            var clientFact = new ApprendaApiClientFactory(url, user, password);

            var apiClient = clientFact.GetV1Client();
            var setting = (await apiClient.GetRegistrySetting("ApplicationRepositoryRoot"));

            return setting.Value.Substring(setting.Value.LastIndexOf("\\\\")).Replace('@', '\\');
        }

        public async Task<string> Test()
        {
            var setting = (await _acpClient.GetRegistrySetting("ApplicationRepositoryRoot"));

            return setting.Value.Substring(setting.Value.LastIndexOf("\\\\")).Replace('@', '\\');
        }

        public async Task<IRestResponse> MakeRestRequest(string resource,
            Method method,
            object body = null,
            string fileName = null,
            string filePath = null,
            HttpStatusCode[] codes = null)
        {
            // Create Rest Request:
            var request = new RestRequest(resource, method);

            if (body != null)
            {
                request.AddJsonBody(body);
            }

            // Perform Rest Request:
            var response = await _acpRestClient.ExecuteTaskAsync(request);

            if (codes != null)
            {
                // If there are specific codes to check against, validate:
                foreach (var code in codes)
                {
                    if (response.StatusCode == code)
                    {
                        return null;
                    }
                }
            }
            else
            {
                // Default codes to evalute:
                if (response.StatusCode == HttpStatusCode.BadRequest &&
                    response.StatusCode == HttpStatusCode.Forbidden)
                {
                    return null;
                }
            }

            return response;

        }

        public string ApprendaSessionToken
        {
            get { return _acpSessionToken; }
        }

        public string ApprendaVersion
        {
            get { return _acpVersion; }
        }

    }
}
