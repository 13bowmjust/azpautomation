﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthorizationPortal.Manager.Data
{
    public class TaskReportCard
    {

        private Dictionary<string, bool> TaskList { get; set; }

        public TaskReportCard()
        {
            TaskList = new Dictionary<string, bool>();
        }

        public TaskReportCard(params string[] tasks)
        {
            TaskList = new Dictionary<string, bool>();

            // Initialize Task Card with defaults:
            foreach (var task in tasks)
            {
                TaskList.Add(task, false);
            }
        }

        public bool GetTaskResult(string task)
        {
            return TaskList[task];
        }

        public void SetTaskResult(string task, bool value)
        {
            TaskList[task] = value;
        }



    }
}
