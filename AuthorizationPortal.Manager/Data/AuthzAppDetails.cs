﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AuthorizationPortal.Manager.Data
{
    public class AuthzAppDetails
    {
        public Dictionary<string, DirectoryInfo> AuthzArchives { get; set; }

        private Dictionary<string, FileInfo> _authzZips { get; set; }

        public AuthzAppDetails()
        {
            AuthzArchives = new Dictionary<string, DirectoryInfo>();
            _authzZips = new Dictionary<string, FileInfo>();
        }

        public AuthzAppDetails(string[] components, string[] directories)
        {
            AuthzArchives = new Dictionary<string, DirectoryInfo>();
            _authzZips = new Dictionary<string, FileInfo>();

            for (var i = 0; i < components.Length; i++)
            {
                AuthzArchives.Add(components[i], new DirectoryInfo(directories[i]));
            }
        }

        ~AuthzAppDetails()
        {
            Dispose();
        }

        public void AddZip(string name, string archive)
        {
            if (_authzZips == null)
            {
                _authzZips = new Dictionary<string, FileInfo>();
            }

            _authzZips.Add(name, new FileInfo(archive));
        }

        public Dictionary<string, FileInfo> AuthzZips
        {
            get { return _authzZips; }
        }

        public void Dispose()
        {
            // Delete all files from the working folder:
            if (AuthzArchives != default(Dictionary<string, DirectoryInfo>))
            {
                AuthzArchives.Values.First().Parent.Parent.Delete(true);
            }

            // Set values to defaults;
            AuthzArchives = new Dictionary<string, DirectoryInfo>();
            _authzZips = new Dictionary<string, FileInfo>();
        }

    }
}
