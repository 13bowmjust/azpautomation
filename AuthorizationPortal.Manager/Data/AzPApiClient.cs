﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Apprenda.ClientServices.AuthorizationPortal.acpClients;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Groups;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Locations;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Permissions;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Roles;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Settings;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Tenants;
using Apprenda.ClientServices.AuthorizationPortal.Domain.TenantTypes;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Users;
using Apprenda.Utility.Extensions;
using Newtonsoft.Json;
using RestSharp;

namespace AuthorizationPortal.Manager.Data
{
    public class AzpApiClient : AcpApiClient
    {
        // AzP API metadata about the types.
        internal class Meta
        {
            /// <summary>
            /// AzP object key fields names, in order.  For example, for Role
            /// is it Type, Id
            /// </summary>
            public string[] Keys { get; set; }
            /// <summary>
            /// The Path from the "api/v1" to this object in the URL for this
            /// type of object
            /// </summary>
            public string Path { get; set; }
            // The plural of this object's name (e.g., "roles").  Lower case.
            // Capitalized as needed when building a message
            public string Plural { get; set; }
            // The singular of this object's name (e.g., "role")
            public string Singular { get; set; }
        }

        // A lookup table for the meta data.
        private readonly Dictionary<Type, Meta> _metaData;


        public AzpApiClient(string cloudUrl,
                            string appAlias,
                            string apiPath = "api/v1")
            : base(cloudUrl, appAlias, apiPath)
        {
            _metaData = new Dictionary<Type, Meta>
            {
                [typeof(Group)] = new Meta()
                {
                    Keys = new[] { "Id" },
                    Path = "groups",
                    Plural = "Groups",
                    Singular = "Group"
                },
                [typeof(GroupUser)] = new Meta()
                {
                    Keys = new[] { "GroupId", "Id" },
                    Path = "groupUsers",
                    Plural = "GroupUsers",
                    Singular = "GroupUser"
                },
                [typeof(Location)] = new Meta()
                {
                    Keys = new[] { "Label" },
                    Path = "locations",
                    Plural = "Locations",
                    Singular = "Location"
                },
                [typeof(Permission)] = new Meta()
                {
                    Keys = new[] { "Name" },
                    Path = "securables/portal",
                    Plural = "Permissions",
                    Singular = "Permission"
                },
                [typeof(Role)] = new Meta()
                {
                    Keys = new[] { "Type", "Id" },
                    Path = "roles",
                    Plural = "Roles",
                    Singular = "Role"
                },
                [typeof(RoleSecurable)] = new Meta()
                {
                    Keys = new[] { "Type", "Id", "AppAlias", "Name" },
                    Path = "roleSecurables",
                    Plural = "RoleSecurables",
                    Singular = "RoleSecurable"
                },
                [typeof(Setting)] = new Meta()
                {
                    Keys = new[] { "Name" },
                    Path = "settings",
                    Plural = "Settings",
                    Singular = "Setting"
                },
                [typeof(Tenant)] = new Meta()
                {
                    Keys = new[] { "Alias" },
                    Path = "tenants",
                    Plural = "Tenants",
                    Singular = "Tenant"
                },
                [typeof(TenantType)] = new Meta()
                {
                    Keys = new[] { "Id" },
                    Path = "tenantTypes",
                    Plural = "TenantTypes",
                    Singular = "TenantType"
                },
                [typeof(TenantTypeRole)] = new Meta()
                {
                    Keys = new[] { "Id", "RoleType", "RoleId" },
                    Path = "tenantTypeRoles",
                    Plural = "TenantTypeRoles",
                    Singular = "TenantTypeRole"
                },
                [typeof(TenantUser)] = new Meta()
                {
                    Keys = new[] { "Alias", "Id" },
                    Path = "tenantUsers",
                    Plural = "TenantUsers",
                    Singular = "TenantUsers"
                },
                [typeof(User)] = new Meta()
                {
                    Keys = new[] { "Id" },
                    Path = "users",
                    Plural = "Users",
                    Singular = "User"
                }
            };
        }

        /// <summary>
        /// The type of operations use to build a URL segment with KeyPath().
        /// </summary>
        /// <remarks>Update is not listed here because it takes
        /// the object in the body and doesn't add to the key path.</remarks>
        private enum CrudOperation
        {
            List,
            Get,
            Delete
        };

        /// <summary>
        /// Build a URL segment from an object.
        /// </summary>
        /// <remarks>If we have a role like:
        ///     {
        ///         Type: "custom",
        ///         Id: "role1",
        ///         Name: "Role 1",
        ///         Description: "A test role",
        ///         Required: 0
        ///     }
        /// and we want to delete it, we need to send a DELETE request to
        ///     cloudUrl/appAlias/api/v1/custom/role1
        /// this builds the "custom/role1" part of that path from the object
        /// by reflection based on the AzP type's metadata.
        /// </remarks>
        /// <param name="op"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        private string KeyPath(CrudOperation op, object obj)
        {
            var m = _metaData[obj.GetType()];

            // The number of parts of the key are needed for this operation
            int keyParts;

            switch (op)
            {
                // To list only custom roles, we pass a role with Type set,
                // the rest of the fields don't matter.
                case CrudOperation.List:
                    keyParts = m.Keys.Length - 1;
                    // FIXME - horrible hack
                    if (obj.GetType() == typeof(TenantTypeRole))
                    {
                        --keyParts;
                    }
                    break;

                // Getting and deleting use the whole key
                case CrudOperation.Get:
                case CrudOperation.Delete:
                    keyParts = m.Keys.Length;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(op), op, null);
            }

            var path = "";
            // Get the value of the key fields by name and add them to the path
            // in order.  In C# we usually do `class.field`.  In JavaScript we 
            // can do either `class.field` or `class["field"]`.  This sort of
            // does the later in C#.
            for (var i = 0; i < keyParts; ++i)
            {
                path += ("/" + obj.GetType().GetProperty(m.Keys[i])?.GetValue(obj));
            }

            return path;
        }

        /// <summary>
        /// Get all items of the specified type from the API and return a
        /// collection of them.
        /// </summary>
        /// <typeparam name="T">The AzP type to retrieve</typeparam>
        /// <param name="template">An instance of the specified type with
        /// all but the last key field filled in</param>
        /// <returns>A collection of ojbect or `null` if there is 
        /// an API failure.</returns>
        public IEnumerable<T> List<T>(T template, bool eus = false)
        {
            var m = _metaData[typeof(T)];
            Console.WriteLine("List<{0}>()", m.Plural);

            var keyPath = "";
            if (template != null)
            {
                keyPath = KeyPath(CrudOperation.List, template);
            }

            // We need the special roles (admin, role sync).  The UI doesn't
            // show them.
            var format = "";
            if (eus || typeof(T) == typeof(User))
            {
                format = "?format=eus";
            }
            var request = new RestRequest($"{m.Path}{keyPath}{format}");

            var response = Execute(request);
            if (!GoodResponse(response, $"list {m.Plural}"))
            {
                return null;
            }

            var results = JsonConvert.DeserializeObject<IEnumerable<T>>(response.Content);

            results = ResolveKeys(template, results);

            return results;
        }

        /// <summary>
        /// Process each item and set key fields from the template if missing
        /// </summary>
        /// <typeparam name="T">The type to process</typeparam>
        /// <param name="template">An item with the key fields set</param>
        /// <param name="items">Items to update</param>
        /// <returns>Items with key fields filled in</returns>
        private IEnumerable<T> ResolveKeys<T>(T template, IEnumerable<T> items)
        {
            if (template == null)
            {
                return items;
            }

            var m = _metaData[typeof(T)];
            var newItems = new List<T>();

            foreach (var item in items)
            {
                // In an old version of the API, listing tenant type roles
                // returned the role ID in the Id field (it was the only field
                // returned).  This is fixed in a later release but in order
                // to support copying from older platforms, we need to fix that
                // here before filling in the tenant type ID below from the 
                // template.  If the RoleId field of a TenantTypeRole is null,
                // this clause swaps it with the Id field.
                if (m.Singular.ToLower().Equals("tenanttyperole")
                    && typeof(T).GetProperty("RoleId")?.GetValue(item) == null)
                {
                    var roleId = typeof(T).GetProperty("Id")?.GetValue(item);
                    typeof(T).GetProperty("RoleId")?.SetValue(item, roleId);
                    typeof(T).GetProperty("Id")?.SetValue(item, null);
                }

                foreach (string key in m.Keys)
                {
                    var value = typeof(T).GetProperty(key)?.GetValue(item);
                    if (value == null)
                    {
                        value = typeof(T).GetProperty(key)?.GetValue(template);
                        typeof(T).GetProperty(key)?.SetValue(item, value);
                    }
                }
                newItems.Add(item);
            }

            return newItems;
        }

        /// <summary>
        /// Get a single item of the specified type from the API and return it.
        /// </summary>
        /// <typeparam name="T">The AzP type to retrieve</typeparam>
        /// <param name="key">An object of the desired type with key fields set</param>
        /// <returns>The object or `default(T)` if there is an API
        /// failure.</returns>
        /// <example>var user = Get&lt;User&gt;("joe@example.com")</example>
        public T Get<T>(T key, bool eus = false)
        {
            var m = _metaData[typeof(T)];
            var keyPath = KeyPath(CrudOperation.Get, key);
            Console.WriteLine("Get<{0}>('{1}')", m.Singular, keyPath);

            // We need all 5 name fields for users.  The UI uses just Name, 
            // the EUS uses prefix, first, etc.
            // We need the special roles (admin, role sync).  The UI doesn't
            // show them.
            var format = "";
            if (eus || typeof(T) == typeof(User))
            {
                format = "?format=eus";
            }
            var request = new RestRequest($"{m.Path}{keyPath}{format}");

            var response = Execute(request);
            if (!GoodResponse(response, $"get {m.Singular}"))
            {
                return default(T);
            }

            return JsonConvert.DeserializeObject<T>(response.Content);
        }

        /// <summary>
        /// Create an item of the specified type via the API.
        /// </summary>
        /// <typeparam name="T">The AzP type to create</typeparam>
        /// <param name="obj">The object to create</param>
        /// <returns>True on success, false otherwise.</returns>
        public bool Create<T>(T obj)
        {
            var m = _metaData[typeof(T)];
            Console.WriteLine("Create<{0}>()", m.Singular);
            var request = new RestRequest(m.Path, Method.POST)
            {
                RequestFormat = DataFormat.Json
            };
            request.AddBody(obj);

            var response = Execute(request);

            return GoodResponse(response, $"create {m.Singular}");
        }

        /// <summary>
        /// Update an item of the specified type via the API.
        /// </summary>
        /// <typeparam name="T">The AzP type to update</typeparam>
        /// <param name="obj">The object to update</param>
        /// <returns>True if successful, false otherwise.</returns>
        public bool Update<T>(T obj)
        {
            var m = _metaData[typeof(T)];
            Console.WriteLine("Update<{0}>()", m.Singular);
            var request = new RestRequest(m.Path, Method.PUT)
            {
                RequestFormat = DataFormat.Json
            };
            request.AddBody(obj);

            var response = Execute(request);

            // Some objects don't have an update method.  When 
            // we get that status, we try to delete and create.
            bool success;
            if (response.StatusCode == HttpStatusCode.MethodNotAllowed)
            {
                success = Delete(obj) && Create(obj);
            }
            else
            {
                success = GoodResponse(response, $"update {m.Singular}");
            }

            return success;

        }

        /// <summary>
        /// Delete an item of the specified type via the API.
        /// </summary>
        /// <typeparam name="T">The AzP type to delete</typeparam>
        /// <param name="obj">The object to delete</param>
        /// <returns>True on success, false otherwise.</returns>
        public bool Delete<T>(T obj)
        {
            var m = _metaData[typeof(T)];
            var keyPath = KeyPath(CrudOperation.Delete, obj);
            Console.WriteLine("Delete<{0}>({1})", m.Singular, keyPath);

            var request = new RestRequest($"{m.Path}{keyPath}", Method.DELETE);

            var response = Execute(request);

            return GoodResponse(response, $"delete {m.Singular}");
        }

        /// <summary>
        /// Load all the objects of the specified type into the AzP database
        /// via the API.
        /// </summary>
        /// <typeparam name="T">The AzP type to load</typeparam>
        /// <param name="source">A collection of objects to load</param>
        /// <param name="stopOnError">True to return on the first error, 
        /// false to try to load as many as possible.</param>
        /// <returns>True if all objects were loaded, false otherwise.</returns>
        /// // FIXME - these actions don't deal with settings or permissions
        public bool LoadFrom<T>(IEnumerable<T> source, bool stopOnError = true)
        {
            var success = true;

            foreach (var item in source)
            {
                // If an item with the same keys doesn't exist, create it.
                if (Get(item) == null)
                {
                    success = success && Create(item);
                }
                // If an item with the same keys exists, try to update it
                // If that fails (some items don't have an update method)
                // then try to delete and craete it.
                else
                {
                    success = success && Update(item);
                }

                if (!success && stopOnError) return false;
            }
            return success;
        }

        /// <summary>
        /// Load all the objects of the specified type into the AzP database
        /// via the API.
        /// </summary>
        /// <typeparam name="T">The AzP type to load</typeparam>
        /// <param name="source">Another AzP instance to copy from.</param>
        /// <param name="template">An instance of the specified type with
        /// all but the last key field filled in</param>
        /// <param name="stopOnError">True to return on the first error, 
        /// false to try to load as many as possible.</param>
        /// <returns>True if all objects were loaded, false otherwise.</returns>
        public bool LoadFrom<T>(AzpApiClient source,
                                T template,
                                bool stopOnError = true)
        {
            return LoadFrom(source.List(template), stopOnError);
        }

        /// <summary>
        /// Copy objects of all AzP types from source portal into this one
        /// </summary>
        /// <remarks>Order is important.  For example, we must create locations
        /// before we can create tenants which use them.
        /// 
        /// This is *additive*, not really a sync (which would remove items not
        /// in the source)</remarks>
        /// <param name="source">The authorization portal to copy from</param>
        /// <param name="stopOnError">True to return on the first error</param>
        /// <returns>True if all objects were copied, false otherwise</returns>
        public bool LoadFrom(AzpApiClient source, bool stopOnError = true)
        {
            var success = true;

            // FIXME - why doesn't this work?
            // Do permissions first because it may affect our ability to manage
            // other objects
            //success = LoadFrom<Permission>(source, stopOnError);
            //if (!success && stopOnError) return false;

            // Locations (These must be done before tenants.)
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            success = success && LoadFrom<Location>(source, null, stopOnError);
            if (!success && stopOnError) return false;

            // Groups
            success = success && LoadFrom<Group>(source, null, stopOnError);
            if (!success && stopOnError) return false;

            // Roles (These must be done before tenant type roles.)
            //
            // There are three types of roles in the AzP database: 
            //  * platform - tenant admin, developer portal user
            //  * portal - authorization admin, etc.
            //  * custom - user-defined roles
            // Platform and portal roles are pre-defined as part of the app and
            // don't need to be loaded/copied.  The following is limited to 
            // custom roles.
            var roleTemplate = new Role()
            {
                Type = "custom"
            };
            var roles = source.List(roleTemplate)
                              .Where(r => !r.Id.EqualsIgnoreCase("syncRoleId")
                                     && !r.Id.EqualsIgnoreCase("adminRoleId")
                                     && !r.Name.Equals("AzP Admin Role"));

            success = success && LoadFrom(roles, stopOnError);
            if (!success && stopOnError) return false;

            // Role securables
            //
            // We only process securables for custom roles.  We generate the
            // list based on the same template used above then process each
            // role from that list.
            // 
            // NOTE: There is a potential optimization with the previous
            // clause.  We could 
            //   1. List the roles from the source
            //   2. Load from roles from the list
            //   3. Iterate over the list here.
            // But premature optimization is the root of all evil.
            var roleSecurable = new RoleSecurable();
            foreach (var role in List(roleTemplate))
            {
                roleSecurable.Type = role.Type;
                roleSecurable.Id = role.Id;

                success = success && LoadFrom(source,
                                              roleSecurable,
                                              stopOnError);
                if (!success && stopOnError) return false;
            }

            // Tenant types (These must be done before tenant type roles and
            // tenants.)
            success = success && LoadFrom<TenantType>(source, null, stopOnError);
            if (!success && stopOnError) return false;

            // Tenant type roles
            // NOTE: There is a potential optimization here of listing tenant 
            // types only once as for roles and role securables (above).
            var tenantTypeRole = new TenantTypeRole();
            foreach (var tenantType in List<TenantType>(null))
            {
                tenantTypeRole.Id = tenantType.Id;
                tenantTypeRole.RoleType = "custom";
                success = success && LoadFrom(source,
                                              tenantTypeRole,
                                              stopOnError);
                if (!success && stopOnError) return false;
            }

            // Users
            success = success && LoadFrom<User>(source, null, stopOnError);
            if (!success && stopOnError) return false;

            // Tenants
            success = success && LoadFrom<Tenant>(source, null, stopOnError);
            if (!success && stopOnError) return false;

            // Tenant users
            // See optimization notes above.
            var tenantUser = new TenantUser();
            foreach (var tenant in List<Tenant>(null))
            {
                tenantUser.Alias = tenant.Alias;
                success = success && LoadFrom(source,
                                              tenantUser,
                                              stopOnError);
                if (!success && stopOnError) return false;
            }

            // Group users
            // See optimization notes above.
            var groupUser = new GroupUser();
            foreach (var group in List<Group>(null))
            {
                groupUser.GroupId = group.Id;
                success = success && LoadFrom(source,
                                              groupUser,
                                              stopOnError);
                if (!success && stopOnError) return false;
            }

            return success;
        }

        /// <summary>
        /// Remove all the objects of the specified type from the AzP database
        /// via the API.
        /// </summary>
        /// <typeparam name="T">The AzP type to remove</typeparam>
        /// <param name="template">An instance of the specified type with
        /// all but the last key field filled in</param>
        /// <param name="stopOnError">True to return on the first error, 
        /// false to try to load as many as possible.</param>
        /// <returns>True if all objects were removed, false otherwise.</returns>
        public bool Clear<T>(T template, bool stopOnError = true)
        {
            var success = true;

            foreach (var item in List(template))
            {
                success = success && Delete(item);
                if (!success && stopOnError) return false;
            }

            return success;
        }

        /// <summary>
        /// Remove all objects of all AzP types
        /// </summary>
        /// <remarks>Order is important.  For example, we must delete tenants
        /// before we can delete the locations they use.</remarks>
        /// <param name="stopOnError">True to return on the first error</param>
        /// <returns>True if all objects were copied, false otherwise</returns>
        public bool Clear(bool stopOnError = true)
        {
            var success = true;

            // Groups (Removing groups automatically removes memberships
            // in the database.)
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            success = success && Clear<Group>(null, stopOnError);
            if (!success && stopOnError) return false;

            // Tenants (Removing tenants automatically removes memberships
            // in the database.)
            success = success && Clear<Tenant>(null, stopOnError);
            if (!success && stopOnError) return false;

            // Tenant types (These must be done after tenants but removing
            // a tenant type removes any tenant type / role assignments
            // automatically in the database.)
            success = success && Clear<TenantType>(null, stopOnError);
            if (!success && stopOnError) return false;

            // We only process custom roles.
            var roleType = new Role()
            {
                Type = "custom"
            };

            // Roles (Removing roles automatically drops user memberships 
            // and any securables assigned to them in the database.)
            success = success && Clear(roleType, stopOnError);
            if (!success && stopOnError) return false;

            // Locations (These must be done after tenants.)
            success = success && Clear<Location>(null, stopOnError);
            if (!success && stopOnError) return false;

            // Users (Must be done last because we may be deleting ourselves.)
            success = success && Clear<User>(null, stopOnError);
            if (!success && stopOnError) return false;

            return success;
        }
    }
}

