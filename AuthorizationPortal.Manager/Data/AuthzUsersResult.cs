﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Users;

namespace AuthorizationPortal.Manager.Data
{
    public class AuthzUsersResult
    {
        public static List<User> FromJson(string json) => JsonConvert.DeserializeObject<List<User>>(json, Converter.Settings);

    }
}
