﻿namespace AuthorizationPortal.Manager.Data
{
    // To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
    //
    //    using QuickType;
    //
    //    var platReportCard = PlatReportCard.FromJson(jsonString);
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class PlatReportCard
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("beginDate")]
        public DateTimeOffset BeginDate { get; set; }

        [JsonProperty("endDate")]
        public DateTimeOffset EndDate { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("sections")]
        public List<Section> Sections { get; set; }
    }

    public partial class Section
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("messages")]
        public List<Message> Messages { get; set; }

        [JsonProperty("beginDate")]
        public DateTimeOffset BeginDate { get; set; }

        [JsonProperty("endDate")]
        public DateTimeOffset EndDate { get; set; }

        [JsonProperty("finished")]
        public bool Finished { get; set; }

        [JsonProperty("canceled")]
        public bool Canceled { get; set; }

        [JsonProperty("operationType")]
        public OperationType OperationType { get; set; }
    }

    public partial class Message
    {
        [JsonProperty("severity")]
        public string Severity { get; set; }

        [JsonProperty("message")]
        public string MessageMessage { get; set; }
    }

    public enum OperationType { Promote };

    public partial class PlatReportCard
    {
        public static List<PlatReportCard> FromJson(string json) => JsonConvert.DeserializeObject<List<PlatReportCard>>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this List<PlatReportCard> self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new OperationTypeConverter(),
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class OperationTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(OperationType) || t == typeof(OperationType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "Promote")
            {
                return OperationType.Promote;
            }
            throw new Exception("Cannot unmarshal type OperationType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (OperationType)untypedValue;
            if (value == OperationType.Promote)
            {
                serializer.Serialize(writer, "Promote"); return;
            }
            throw new Exception("Cannot marshal type OperationType");
        }
    }
}
