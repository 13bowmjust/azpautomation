﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Tenants;
using Apprenda.ClientServices.AuthorizationPortal.Domain.TenantTypes;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Locations;
using Apprenda.ClientServices.AuthorizationPortal.Domain.Users;

namespace AuthorizationPortal.Manager.Data
{

    public enum AuthzWorkflow { New, Upgrade }

    public enum LogLevel { Fatal, Warning, Info }

    public static class Helper
    {


        public static string[][] AuthzWorkflowData = new string[][]
        {
            new string[] {"core", "auth", "eus" },
            new string[] { "patch", "core", "eus" }
        };

        public static bool Copy(string sourceDirectory, string targetDirectory)
        {
            DirectoryInfo diSource = new DirectoryInfo(sourceDirectory);
            DirectoryInfo diTarget = new DirectoryInfo(targetDirectory);

            var resultCheck = true;

            CopyAll(diSource, diTarget, out resultCheck);

            return resultCheck;
        }

        public static void CopyAll(DirectoryInfo source, DirectoryInfo target, out bool result)
        {
            try
            {
                Directory.CreateDirectory(target.FullName);

                // Copy each file into the new directory.
                foreach (FileInfo fi in source.GetFiles())
                {
                    fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);
                }

                // Copy each subdirectory using recursion.
                foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
                {
                    DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                    CopyAll(diSourceSubDir, nextTargetSubDir, out result);
                }

                result = true;
            }
            catch
            {
                result = false;
            }
        }

        public static string ToLogFormat(this string value, LogLevel level)
        {
            // Convert the calling string into a formatted log value:
            // Format: [DateTime]:|LEVEL|: >> {value}
            var formatLevel = level.ToString().ToUpper();
            var formatDate = DateTime.Now.ToString("u");

            return $"[{formatDate}]:|{formatLevel}|: >> {value}";
        }

        public static bool ContainsAny(this string str, params string[] values)
        {
            if (!string.IsNullOrEmpty(str) || values.Length > 0)
            {
                foreach (string value in values)
                {
                    if (str.ToLower().Contains(value.ToLower()))
                        return true;
                }
            }

            return false;
        }

        public static Tenant ToAzPTenant(this TenantItem tenant,
            TenantType tenantType,
            Location location,
            string state = "approved",
            string description = " ")
        {
            return new Tenant()
            {
                Alias = tenant.Alias,
                Description = description,
                Name = tenant.Name,
                State = state,
                Type = tenantType.Id,
                TypeDescription = tenantType.Description,
                Location = location.Description,
                Label = location.Label
            };
        }

        public static User ToAzPUser(this AdminUser user, string description = " ")
        {
            return new User()
            {
                Email = user.Email,
                Id = user.Identifier,
                Description = description,
                FirstName = user.FirstName,
                LastName = user.LastName,
                MiddleName = " ",
                Prefix = " ",
                Suffix = " "
            };
        }

        public static User ToAzPUser(this Item user, string description = " ")
        {
            return new User()
            {
                Email = user.Email,
                Id = user.Identifier,
                Description = description,
                FirstName = user.FirstName,
                LastName = user.LastName,
                MiddleName = " ",
                Prefix = " ",
                Suffix = " "
            };
        }

    }
}
