﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var socUsers = SocUsers.FromJson(jsonString);

namespace AuthorizationPortal.Manager.Data
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public partial class PlatSocUsers
    {
        [JsonProperty("currentPage")]
        public long CurrentPage { get; set; }

        [JsonProperty("pageSize")]
        public long PageSize { get; set; }

        [JsonProperty("totalPages")]
        public long TotalPages { get; set; }

        [JsonProperty("totalItems")]
        public long TotalItems { get; set; }

        [JsonProperty("items")]
        public List<Item> Items { get; set; }

        [JsonProperty("nextPage")]
        public object NextPage { get; set; }

        [JsonProperty("previousPage")]
        public object PreviousPage { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }
    }

    public partial class Item
    {
        [JsonProperty("identifier")]
        public string Identifier { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("isEnabled")]
        public bool IsEnabled { get; set; }

        [JsonProperty("memberOfTenants")]
        public List<MemberOfTenant> MemberOfTenants { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }
    }

    public partial class MemberOfTenant
    {
        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("isTenantEnabled")]
        public bool IsTenantEnabled { get; set; }

        [JsonProperty("isUserEnabledForTenant")]
        public bool IsUserEnabledForTenant { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }
    }

    public partial class PlatSocUsers
    {
        public static PlatSocUsers FromJson(string json) => JsonConvert.DeserializeObject<PlatSocUsers>(json, Converter.Settings);
    }

}

