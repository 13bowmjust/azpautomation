﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var platSocTenants = PlatSocTenants.FromJson(jsonString);

namespace AuthorizationPortal.Manager.Data
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public partial class PlatSocTenants
    {
        [JsonProperty("currentPage")]
        public long CurrentPage { get; set; }

        [JsonProperty("pageSize")]
        public long PageSize { get; set; }

        [JsonProperty("totalPages")]
        public long TotalPages { get; set; }

        [JsonProperty("totalItems")]
        public long TotalItems { get; set; }

        [JsonProperty("items")]
        public List<TenantItem> TenantItems { get; set; }

        [JsonProperty("nextPage")]
        public object NextPage { get; set; }

        [JsonProperty("previousPage")]
        public object PreviousPage { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }
    }

    public partial class TenantItem
    {
        [JsonProperty("numberOfUsers")]
        public long NumberOfUsers { get; set; }

        [JsonProperty("numberOfApps")]
        public long NumberOfApps { get; set; }

        [JsonProperty("isEnabled")]
        public bool IsEnabled { get; set; }

        [JsonProperty("adminUser")]
        public AdminUser AdminUser { get; set; }

        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }
    }

    public partial class AdminUser
    {
        [JsonProperty("identifier")]
        public string Identifier { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("isEnabled")]
        public bool IsEnabled { get; set; }

        [JsonProperty("memberOfTenants")]
        public object MemberOfTenants { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }
    }

    public partial class PlatSocTenants
    {
        public static PlatSocTenants FromJson(string json) => JsonConvert.DeserializeObject<PlatSocTenants>(json, Converter.Settings);
    }
}

